import random
def max_number(num1, num2 , num3):
    if num1 > num2 and num1 > num3:
        return num1
    elif num2 > num1 and num2 > num3:
        return num2
    else:
        return num3

for num in range(100):
    num1 = random.randint(1,500)
    num2 = random.randint(1,500)
    num3 = random.randint(1,500)
    result = max_number(num1, num2, num3)
    print "Max of %d,%d and %d is %d" % (num1, num2, num3, result)



